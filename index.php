<?php
class Hewan{

    public $nama,
            $darah,
            $jumlahkaki,
            $keahlian;

		public function __construct($nama,$darah,$jumlahkaki,$keahlian){
			$this->nama = $nama;
			$this->darah = $darah;
			$this->jumlahkaki = $jumlahkaki;
			$this->keahlian = $keahlian;
		}  	
		
		public function getInfoHewan(){
			return "Nama Hewan : $this->nama,
					<br> Darah : $this->darah,
					<br> Jumlah Kaki : $this->jumlahkaki,
					<br> Keahlian : $this->keahlian <br>";
		}	
		public function atraksi(){
			return "$this->nama, sedang $this->keahlian";
    }
	
}

	class Fight{
	
		public $attackPower,
				$defencePower;
				
		public function __construct($attackPower,$defencePower){
			$this->attackPower = $attackPower;
			$this->defencePower = $defencePower;			
		}
			
			public function atraksi_2(){
			return "attackPower : $this->attackPower <br> defencePower : $this->defencePower <br>";
			}
			public function serang(){
				return "Harimau sedang menyerang Elang";
					
      }
	  
	  public function diserang(){
		  $this->darah = 50;
		  return "mengurangi darahnya menjadi " . $this->darah-($this->attackPower/$this->defencePower);
	  }
}



$harimau = new Hewan("Harimau", 50, 4, "Lari Cepat");
$elang = new Hewan("Elang", 50, 2, "Terbang Tinggi");
$harimau_1 = new Fight(7,8);
$elang_1 = new Fight(10,5);
echo $harimau->getInfoHewan();
echo $harimau_1->atraksi_2();
echo "<br>";
echo $elang->getInfoHewan();
echo $elang_1->atraksi_2();
echo "<br>";
echo "Nama Hewan : " . $harimau->atraksi();
echo "<br>";
echo "Nama Hewan : " . $elang->atraksi();
echo "<br>";
echo "<br>";
$harimau_2 = new fight(7,8);
echo $harimau_2->serang();
echo "<br>";
echo $harimau_2->diserang();
?>